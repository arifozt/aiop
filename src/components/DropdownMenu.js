
import React, { useState, useEffect, useRef } from 'react';
import { CSSTransition } from 'react-transition-group';
import { Button} from 'react-bootstrap';
import SourceList from './SourceList'
import {ReactComponent as LeftArrowIcon} from '../resources/arrow.svg';
import {ReactComponent as TickIcon} from '../resources/tick.svg';
import {ReactComponent as SourcesIcon} from '../resources/sources.svg';

import '../index.css';


const sources = [
  { id: 1, name: "Reddit", icon: "R"},
  { id: 2, name: "Facebook", icon: "F" },
  { id: 3, name: "Twitter", icon: "T" },
  { id: 4, name: "YouTube", icon: "Y" },
  { id: 5, name: "ESPN", icon: "E" },
  { id: 6, name: "Twitch", icon: "T" },
  { id: 7, name: "The Guardian", icon: "G" },
  { id: 8, name: "BBC", icon: "B" },
  { id: 9, name: "Sky", icon: "S" },
  { id: 10, name: "Metro", icon: "M" }
];



function DropdownMenu() {
  const [pickedSources, setPickedSources] = useState([]);
  const [allSources,setAllSources] = useState(sources);
  const [filteredSources,setFilteredSources] = useState(allSources);
  const [activeMenu, setActiveMenu] = useState('main');
  const [menuHeight, setMenuHeight] = useState(null);
  const dropdownRef = useRef(null);
  let searchTerm = ""
  
  useEffect(() => {
    setMenuHeight(dropdownRef.current?.firstChild.offsetHeight)
  }, [])

  function calcHeight(el) {
    const height = el.offsetHeight;
    setMenuHeight(height);
  }

  function DropdownItem(props) {
    return (
      <a href="#" className="menu-item" onClick={() => props.goToMenu && setActiveMenu(props.goToMenu)}>
        <span className="icon-button">{props.leftIcon}</span>
        {props.children}
      </a>
    );
  }
  function handleSearchChange(event){
	setAllSources(sources);
	let value = event.target.value.toLowerCase();
	if(value.length < 1)
	{
			setFilteredSources(sources);
		return;
	}
	let result = allSources.filter((source) => { return source.name.toLowerCase().search(value) != -1; });
  	setFilteredSources(result)
  }
  
  function handleClickedSource(event){
	let pickedSource = event.target.innerText;
	if(pickedSource.includes('\n'))
		pickedSource = pickedSource.slice(2)

	const pickedElement = allSources.find((element) => {return element.name == pickedSource;});
	if(pickedSources.find((element) => {return element.name == pickedSource;}) == undefined)
	{
		let newSources = Array.from(allSources)
		let newFilteredSources = Array.from(allSources)
		const newSourcesIndex = newSources.indexOf(pickedElement)
		const newFilteredSourcesIndex = newFilteredSources.indexOf(pickedElement)
		newSources[newSourcesIndex].icon = <TickIcon/>;
		newFilteredSources[newFilteredSourcesIndex].icon = <TickIcon/>;
		setAllSources(newSources);
		setFilteredSources(newFilteredSources);
  		pickedSources.push({id: pickedSources.length + 1,name: pickedSource, icon: pickedSource[0]});
  	}
	else
	{
		let newSources = Array.from(allSources)
		let newFilteredSources = Array.from(allSources)
		const newSourcesIndex = newSources.indexOf(pickedElement)
		const newFilteredSourcesIndex = newFilteredSources.indexOf(pickedElement)
		newSources[newSourcesIndex].icon = newSources[newSourcesIndex].name[0];
		newFilteredSources[newFilteredSourcesIndex].icon = newFilteredSources[newFilteredSourcesIndex].name[0];
		setAllSources(newSources);
		setFilteredSources(newFilteredSources);
		pickedSources.splice(pickedSources.indexOf(pickedSources.find((element) => {return element.name == pickedSource;})), 1);
	}
  }
  return (
    <div className="dropdown" style={{ height: menuHeight }} ref={dropdownRef}>
      <CSSTransition
        in={activeMenu === 'main'}
        timeout={500}
        classNames="menu-primary"
        unmountOnExit
        onEnter={calcHeight}>
        <div className="menu">
          <DropdownItem leftIcon = <LeftArrowIcon/>>
            <h2>Profile</h2>
          </DropdownItem>
          <DropdownItem
            leftIcon=<SourcesIcon/>
            goToMenu="Sources">
            Sources
          </DropdownItem>

        </div>
      </CSSTransition>

      <CSSTransition
        in={activeMenu === 'Sources'}
        timeout={500}
        classNames="menu-secondary"
        unmountOnExit
        onEnter={calcHeight}>
        <div className="menu">
          <DropdownItem goToMenu="main" leftIcon = <LeftArrowIcon/>>
            <h2>Sources</h2>
          </DropdownItem>
	  <SourceList sources={pickedSources}/>
	  <DropdownItem leftIcon="+" goToMenu="Add Sources">Manage Sources</DropdownItem>
        </div>
      </CSSTransition>
      
      <CSSTransition
        in={activeMenu === 'Add Sources'}
        timeout={500}
        classNames="menu-secondary"
        unmountOnExit
        onEnter={calcHeight}>
        <div className="menu">
          <DropdownItem goToMenu="Sources"  leftIcon = <LeftArrowIcon/>>
            <h2>Add Sources</h2>
          </DropdownItem>
  	  <label>
    		Site:
    		<input type="text" name="site" id="site" onChange={handleSearchChange}/>
  	  </label>
	  <SourceList sources={filteredSources} onClick={handleClickedSource}/>
        </div>
      </CSSTransition>

    </div>
  );
}

export default DropdownMenu;