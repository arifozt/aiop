import React from "react";

// import the Contact component
import Source from "./Source";


function SourceList(props) {
  return (
    <div>
      {props.sources.map(s => <Source key={s.id} name={s.name} onClick={props.onClick} icon={s.icon}/>)};
     </div> 
  ); 
} 

export default SourceList;