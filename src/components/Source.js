import React, { useState, useEffect, useRef } from 'react';
import {ReactComponent as TickIcon} from '../resources/tick.svg';
import './Source.css';
function Source(props) {
  function changeIcon()
  {
	if(typeof(props.icon) != "string")
	{
		props.icon = props.name[0];
	}
	else
	{
		props.icon = <TickIcon/>;
	}
  }
  return (
    <div className="source" onClick={props.onClick} icon={props.icon}>
	<span className="icon-button">{props.icon}</span>
	<span>{props.name}</span>
    </div>
  );
}
export default Source;
