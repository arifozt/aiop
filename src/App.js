import './index.css';

import Navbar from './components/Navbar'
import NavItem from './components/NavItem'
import DropdownMenu from './components/DropdownMenu'
import {ReactComponent as ProfileIcon} from './resources/profile.svg';
import {ReactComponent as HomeIcon} from './resources/home.svg';

import React, { useState, useEffect, useRef } from 'react';


function App() {
  const [pickedSources, setPickedSources] = useState([]);
  console.log(pickedSources);		
  return (
	<Navbar> 
		<NavItem icon=<HomeIcon/>/> 
		<NavItem icon=<ProfileIcon/>> 
			<DropdownMenu pickedSources = {pickedSources} setPickedSources = {setPickedSources}></DropdownMenu>	
		</NavItem>
	</Navbar>
  );
}

export default App;
